﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace wwww.Controllers
{
    [Authorize]
    public class WwwwController : Controller
    {
        // GET: Wwww
        public ActionResult Index()
        {
            return View();
        }
    }
}