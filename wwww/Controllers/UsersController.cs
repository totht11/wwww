﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using wwww.DTO;
using wwww.Models;

namespace wwww.Controllers
{
    public class UsersController : ApiController
    {
        public IQueryable<User> Get()
        {
            return ApplicationDbContext.Create().Users.Select(u => new User { Id = u.Id, NickName = u.NickName });
        }
    }
}
