﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EscherClient;
using Microsoft.AspNet.Identity;
using wwww.Proxy;
using WwwwLogger;

namespace wwww.Controllers
{
    [Authorize]
    public class ProxyController : ApiController
    {

        private readonly WwwwProxy proxy = new WwwwProxy();


        [AcceptVerbs("GET", "PUT", "POST", "PATCH", "DELETE")]
        [Route("api/{*apiRoute:regex(^(?!.*Users))}")]
        public async Task<HttpResponseMessage> HandleUiRequest(string apiRoute, CancellationToken cancellationToken)
        {
            return await proxy.ProxyRequest(apiRoute, Request, UserId, cancellationToken);
        }


        [AcceptVerbs("GET", "PUT", "POST", "PATCH", "DELETE")]
        [Route("backend/api/{*apiRoute}")]
        public async Task<HttpResponseMessage> HandleApiRequest(string apiRoute, CancellationToken cancellationToken)
        {
            return await proxy.ProxyRequest(apiRoute, Request, UserId, cancellationToken);
        }


        private string UserId => User.Identity.GetUserId();

    }
}
