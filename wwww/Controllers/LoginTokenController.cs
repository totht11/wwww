﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using wwww.DTO;
using wwww.Models;
using WwwwLogger;

namespace wwww.Controllers
{
    public class LoginTokenController : ApiController
    {

        private readonly ApplicationDbContext db = new ApplicationDbContext();


        [HttpPost]
        [Route("api/Users/LoginTokens")]
        public async Task<HttpResponseMessage> LogIn(LoginTokenCreationParameter credentials)
        {
            var userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();

            var user = await userManager.FindByNameAsync(credentials.Email);
            if (user == null)
            {
                Logger.Warn(Logger.Message("LoginToken creation failed: no such user").AddParameter("Email", credentials.Email));
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid creadentials"));
            }

            var isPasswordCorrect = await userManager.CheckPasswordAsync(user, credentials.Password);
            if (!isPasswordCorrect)
            {
                Logger.Warn(Logger.Message("LoginToken creation failed: incorrect password").AddParameter("Email", credentials.Email).AddParameter("UserId", user.Id));
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid creadentials"));
            }

            var loginToken = new LoginToken()
            {
                UserId = user.Id,
                Token = GenerateNewLoginToken(),
                Created = DateTime.Now
            };
            db.LoginTokens.Add(loginToken);

            await db.SaveChangesAsync();
            Logger.Info(Logger.Message("LoginToken creation successfull.").AddParameter("Email", credentials.Email).AddParameter("UserId", user.Id));

            return Request.CreateResponse(HttpStatusCode.Created, loginToken);
        }


        [Authorize]
        [HttpDelete]
        [Route("api/Users/LoginTokens")]
        public async Task<HttpResponseMessage> LogIn()
        {
            var token = Request.Headers.GetValues("X-WWWW-LoginToken").Single();
            var loginToken = await db.LoginTokens.SingleAsync(lt => lt.Token == token);

            db.LoginTokens.Remove(loginToken);

            await db.SaveChangesAsync();
            Logger.Info(Logger.Message("LoginToken delete successfull.").AddParameter("Token", token).AddParameter("UserId", loginToken.UserId));

            return Request.CreateResponse(HttpStatusCode.NoContent);
        }


        [NonAction]
        private static string GenerateNewLoginToken()
        {
            string newLoginToken;
            using (RandomNumberGenerator random = new RNGCryptoServiceProvider())
            {
                byte[] tokenData = new byte[48];
                random.GetBytes(tokenData);

                newLoginToken = Convert.ToBase64String(tokenData);
            }
            
            return newLoginToken;
        }
    }
}
