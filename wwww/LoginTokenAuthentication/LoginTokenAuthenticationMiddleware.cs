﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using wwww.Models;

using AppFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;


namespace wwww.LoginTokenAuthentication
{
    public class LoginTokenAuthenticationMiddleware
    {
        private AppFunc next;

        public void Initialize(AppFunc next)
        {
            this.next = next;
        }

        public async Task Invoke(IDictionary<string, object> environment)
        {
            var context = new OwinContext(environment);
            var loginTokenFromHeader = context.Request.Headers.Get("X-WWWW-LoginToken");
            var db = context.Get<ApplicationDbContext>();

            if (loginTokenFromHeader != null)
            {
                var loginToken = await db.LoginTokens.SingleOrDefaultAsync(lt => lt.Token == loginTokenFromHeader);
                if (loginToken != null)
                {
                    var claimsIdentity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.NameIdentifier, loginToken.UserId) }, Constants.DefaultSecurityStampClaimType);
                    context.Authentication.SignIn(claimsIdentity);

                    context.Request.User = new GenericPrincipal(claimsIdentity, new string[] { });
                }
            }

            await next.Invoke(environment);
        }
    }
}
