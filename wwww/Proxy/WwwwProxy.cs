﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using EscherClient;
using WwwwLogger;

namespace wwww.Proxy
{
    public class WwwwProxy
    {

        private readonly EscherHttpClient client = new EscherHttpClient();


        public async Task<HttpResponseMessage> ProxyRequest(string apiRoute, HttpRequestMessage request, string userId, CancellationToken cancellationToken)
        {
            Logger.Debug(Logger.Message("request proxied from web ui").AddParameter("RequestUri", request.RequestUri.ToString()));

            var uri = new UriBuilder(request.RequestUri);
            uri.Path = uri.Path.Replace("backend/", "");

            if (apiRoute.StartsWith("Events"))
            {
                uri.Port = 10001;
            }
            else if (apiRoute.StartsWith("Users"))
            {
                uri.Port = 10000;
            }
            else if (apiRoute.StartsWith("Recommendations"))
            {
                uri.Port = 10004;
            }
            else
            {
                Logger.Warn(Logger.Message("request proxy failed: no such route").AddParameter("RequestUri", request.RequestUri.ToString()));
                throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.NotFound, "no such route"));
            }

            var proxyRequest = new HttpRequestMessage
            {
                RequestUri = uri.Uri,
                Method = request.Method
            };

            if (request.Method == HttpMethod.Post || request.Method == HttpMethod.Put || request.Method == new HttpMethod("PATCH"))
            {
                proxyRequest.Content = request.Content;
            }

            proxyRequest.Headers.Add("X-WWWW-UserId", userId);

            return await client.SendAsync(proxyRequest, cancellationToken);
        }

    }
}