'use strict';

// let angular = require('angular');

class EventService {

    constructor(backendApiService, parameterService) {
        this._backendApiService = backendApiService;
        this._parameterService = parameterService;

        this.eventsCache = {};
    }


    save(eventData) {
        return this._backendApiService
            .post('/api/Events', eventData)
            .then((response) => {this.refreshCaches(); return response;}, (response) => console.log(response));
    }


    loadAll(criteria) {
        if (!this.eventsCache.hasOwnProperty(JSON.stringify(criteria))) {
            this.reloadAll(criteria);
        }
    }


    reloadAll(criteria) {
        let cacheKey = JSON.stringify(criteria);
        this.eventsCache[cacheKey] = [];

        return this._backendApiService
            .get('/api/Events', {params: criteria})
            .then((response) => this.eventsCache[cacheKey] = response.data.map(this.transformCoordinates), (response) => console.log(response));
    }


    refreshCaches() {
        angular.forEach(this.eventsCache, (value, key) => {
            this.reloadAll(JSON.parse(key));
        });
    }


    loadOne(eventId) {
        return this._backendApiService
            .get(`/api/Events/${eventId}`)
            .then(r => this.transformCoordinates(r.data), (response) => console.log(response));
    }


    eventsList(criteria) {
        return this.eventsCache[JSON.stringify(criteria)];
    }


    signUp(eventId) {
        return this._backendApiService
            .post(`/api/Events/${eventId}/Subscriptions`)
            .then(r => r, (response) => console.log(response));
    }


    cancelAttendance(eventId, userId) {
        if (!userId) {
            userId = this._parameterService.userId;
        }

        return this._backendApiService
            .delete(`/api/Events/${eventId}/Subscriptions?UserId=${userId}`)
            .then(r => r, (response) => console.log(response));
    }


    isSignedUp(eventId) {
        return this._backendApiService
            .get(`/api/Events/${eventId}/Subscriptions?UserId=${this._parameterService.userId}`)
            .then((response) => response.data.length > 0, (response) => response);
    }


    getParticipants(eventId) {
        return this._backendApiService
            .get(`/api/Events/${eventId}/Subscriptions`)
            .then((response) => response.data, (response) => console.log(response));
    }
    
    
    transformCoordinates(event) {
        if (event.Lat && event.Lon) {
            event.mapCoordinates = {lat: event.Lat, lng: event.Lon};
            event.mapMarkers = [{position: event.mapCoordinates, title: event.Title}];
        }
        
        return event;
    }


    static create() {
        return ['backendApiService', 'parameterService', EventService];
    }

}

module.exports = EventService;
