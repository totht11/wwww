'use strict';

class EventRecommendationsService {

    constructor(backendApiService, eventService) {
        this._backendApiService = backendApiService;
        this._eventService = eventService;
        this.recommendations = null;
        this.loadPromise = null;
        this.lastLoaded = new Date();
    }


    load() {
        // load new recommendations every 10 minutes
        if (this.loadPromise == null || (new Date().getTime() - this.lastLoaded.getTime()) > 10 * 60 * 1000) {
            this.recommendations = null;
            this.lastLoaded = new Date();

            this.loadPromise = this.reload();
            return this.loadPromise;
        }

        return this.loadPromise;
    }


    reload() {
        return this._backendApiService
            .get('/api/Recommendations/Events')
            .then((response) => this.recommendations = response.data.map(this._eventService.transformCoordinates), (response) => console.log(response));
    }


    get list() {
        return this.recommendations;
    }


    static create() {
        return ['backendApiService', 'eventService', EventRecommendationsService];
    }

}

module.exports = EventRecommendationsService;
