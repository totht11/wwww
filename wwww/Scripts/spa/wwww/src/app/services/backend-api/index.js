'use strict';

class BackendApiService {

    constructor($http, parameterService) {
        this._$http = $http;
        this._parameterService = parameterService;

        if (parameterService.loginToken) {
            $http.defaults.headers.common['X-WWWW-LoginToken'] = parameterService.loginToken;
        }
    }


    get() {
        return this._$http.get.apply(this._$http, this._addBackendHostToFirstArgument(arguments));
    }


    post() {
        return this._$http.post.apply(this._$http, this._addBackendHostToFirstArgument(arguments));
    }


    put() {
        return this._$http.put.apply(this._$http, this._addBackendHostToFirstArgument(arguments));
    }


    patch() {
        return this._$http.patch.apply(this._$http, this._addBackendHostToFirstArgument(arguments));
    }


    delete() {
        return this._$http.delete.apply(this._$http, this._addBackendHostToFirstArgument(arguments));
    }


    _addBackendHostToFirstArgument(originalArguments) {
        if (!this._parameterService.backendApi)
            return originalArguments;

        originalArguments[0] = this._parameterService.backendApi + originalArguments[0];
        return originalArguments;
    }


    static create() {
        return ['$http', 'parameterService', BackendApiService];
    }

}

module.exports = BackendApiService;
