'use strict';

class EventCategoryService {

    constructor(backendApiService) {
        this._backendApiService = backendApiService;
        this._commentsByEventId = {};
    }


    reload(eventId) {
        this._commentsByEventId[eventId] = [];

        return this._backendApiService
          .get(`/api/Events/${eventId}/Comments`)
          .then((response) => this._commentsByEventId[eventId] = response.data, (response) => console.log(response));
    }


    create(eventId, userId, message) {
        return this._backendApiService
            .post(`/api/Events/${eventId}/Comments`, {EventId: eventId, UserId: userId, Message: message})
            .then((response) => response, (response) => console.log(response));
    }


    delete(eventId, commentId) {
        return this._backendApiService
            .delete(`/api/Events/${eventId}/Comments/${commentId}`)
            .then((response) => response, (response) => console.log(response));
    }


    list(eventId) {
        return this._commentsByEventId[eventId];
    }


    static create() {
        return ['backendApiService', EventCategoryService];
    }

}

module.exports = EventCategoryService;
