'use strict';

class EventCategoryService {

    constructor(backendApiService) {
        this._backendApiService = backendApiService;
        this.eventCategories = [];
        this.loadPromise = null;
    }


    load() {
        if (this.loadPromise == null) {
            this.loadPromise = this.reload();

            return this.loadPromise;
        }

        return this.loadPromise;
    }


    reload() {
        return this._backendApiService
          .get('/api/Events/Categories')
          .then((response) => this.eventCategories = response.data, (response) => console.log(response));
    }


    get list() {
        return this.eventCategories;
    }


    static create() {
        return ['backendApiService', EventCategoryService];
    }

}

module.exports = EventCategoryService;
