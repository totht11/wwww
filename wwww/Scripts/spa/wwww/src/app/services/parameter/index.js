'use strict';

let angular = require('angular');

class ParameterService {

    constructor($window) {
        let parameters = $window.wwww.parameters;
        angular.extend(this, parameters);
    }


    static create() {
        return ['$window', ParameterService];
    }

}

module.exports = ParameterService;
