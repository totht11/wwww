'use strict';

class UserService {

    constructor(backendApiService, $q) {
        this._backendApiService = backendApiService;
        this._$q = $q;

        this.users = [];
        this.loadCalled = false;
    }


    load() {
        if (this.loadCalled == false) {
            this.loadCalled = true;

            return this.reload();
        }

        return this._$q.resolve();
    }


    reload() {
        return this._backendApiService
          .get('/api/Users')
          .then((response) => this.users = response.data, (response) => console.log(response));
    }


    get list() {
        return this.users;
    }


    static create() {
        return ['backendApiService', '$q', UserService];
    }

}

module.exports = UserService;
