'use strict';

class GeoLocationService {

    constructor($window, $q) {
        this._$window = $window;
        this._$q = $q;
    }


    getCurrentPosition() {
        if (!this._$window.navigator.geolocation) {
            return this._$q.reject();
        }

        var result = this._$q.defer();

        this._$window.navigator.geolocation.getCurrentPosition(
            (position)  => {
                result.resolve(position);
            },
            (error) => {
                result.reject(error);
            }
        );

        return result.promise;
    }


    static create() {
        return ['$window', '$q', GeoLocationService];
    }

}

module.exports = GeoLocationService;
