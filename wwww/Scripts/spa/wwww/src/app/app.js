'use strict';

let angular = require('angular');
require('angular-route');
require('angular-ui-bootstrap');

let wwww = angular
  .module('wwww', ['ngRoute', 'ui.bootstrap'])
  .directive({
    user: require('./directives/user').create(),
    userProfile: require('./directives/user-profile').create(),
    homePage: require('./directives/home-page').create(),

    eventRecommendations: require('./directives/event-recommendations').create(),
    eventPage: require('./directives/event-page').create(),
    eventList: require('./directives/event-list').create(),
    eventForm: require('./directives/event-form').create(),
    eventDetails: require('./directives/event-details').create(),
    eventComments: require('./directives/event-comments').create(),
    eventCategory: require('./directives/event-category').create(),
    googleMaps: require('./directives/google-maps').create()
  })
  .filter({
    robohash: require('./filters/robohash').create(),
    urlencode: () => window.encodeURIComponent
  })
  .service({
    userService: require('./services/user').create(),
    parameterService: require('./services/parameter').create(),
    backendApiService: require('./services/backend-api').create(),

    eventService: require('./services/event').create(),
    eventCategoryService: require('./services/event-category').create(),
    eventRecommendationsService: require('./services/event-recommendations').create(),
    eventCommentService: require('./services/event-comment').create(),

    countryService: require('./services/country').create(),
    geoLocationService: require('./services/geolocation').create()
  })
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/home', {
        template: require('./home.jade')
      })
      .when('/events', {
        template: require('./events.jade')
      })
      .when('/events/new', {
        template: require('./events-new.jade')
      })
      .when('/events/:eventId', {
        template: require('./events-details.jade')
      })
      .when('/users/:userNickName', {
        template: require('./user-profile.jade')
      })
      .otherwise({
        redirectTo: '/home'
      });
  }]);

angular.element(document).ready(() => {
  angular.bootstrap(document, ['wwww']);
});
