'use strict';

class EventFormController {

  constructor(eventService, eventCategoryService, countryService, $location) {
    this._eventService = eventService;
    this._countryService = countryService;
    this._$location = $location;
    this._eventCategoryService = eventCategoryService;
    this._eventCategoryService.load();

    this.setupDateAndTimePicker();

    this.event = {
      Duration: 30,
      DurationUnit: 'Minutes'
    };
  }


  setupDateAndTimePicker() {
    var now = new Date();
    now.setSeconds(0, 0);
    this.date = this.time = now;

    this.dateOptions = {
      maxDate: new Date(2020, 5, 22),
      minDate: new Date(),
      initDate: new Date(),
      startingDay: 1
    };
  }


  save() {
    this.event.StartTime = new Date(this.date.getTime());
    this.event.StartTime.setHours(this.time.getHours(), this.time.getMinutes(), this.time.getSeconds());

    this._eventService.save(this.event)
      .then((response) => this._$location.path(`/events/${response.data.Id}`));
  }


  get categoryOptions() {
    return this._eventCategoryService.list;
  }


  get countries() {
    return this._countryService.list;
  }


  static create() {
    return () => ({
      template: require('./index.jade'),
      restrict: 'E',
      scope: {},
      replace: false,
      bindToController: true,
      controller: ['eventService', 'eventCategoryService', 'countryService', '$location', EventFormController],
      controllerAs: 'controller',
      controllerClass: EventFormController
    });
  };

}

module.exports = EventFormController;
