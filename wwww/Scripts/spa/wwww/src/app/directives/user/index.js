'use strict';

class UserNameController {

    constructor(userService) {
        this._userService = userService;
        this._userService.load();
    }


    get userName() {
        return (this._userService.list.filter(c => c.Id == this.userId)[0] || {NickName: ""}).NickName;
    }


    static create() {
        return () => ({
            template: require('./index.jade'),
            restrict: 'E',
            scope: {userId: "=userId", picture: "=picture"},
            replace: false,
            bindToController: true,
            controller: ['userService', UserNameController],
            controllerAs: 'controller',
            controllerClass: UserNameController
        });
    };

}

module.exports = UserNameController;
