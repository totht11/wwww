'use strict';

class EventDetailsController {

    constructor(eventService, parameterService, $routeParams) {
        this._parameterService = parameterService;
        this.eventId = $routeParams.eventId;
        this.userId = this._parameterService.userId;

        this._eventService = eventService;
        this._eventService.loadOne(this.eventId)
            .then((event) => {
                this.event = event;
                this.eventCoordinates = event.mapCoordinates;
                this.mapMarkers = event.mapMarkers;
            });
        this._eventService.getParticipants(this.eventId)
            .then((participants) => {
                this.isSignedUp = participants.filter(p => p.UserId == this.userId).length > 0;
                this.participants = participants;
            });

        this.participants = [];
        this.event = {Id: this.eventId};
        this.isSignedUp = null;
    }


    signUp() {
        this._eventService.signUp(this.eventId).then(() => {
            this.isSignedUp = true;
            this.participants.push({UserId: this.userId});
        });
    }


    cancelAttendance(userId) {
        if (!userId) {
            userId = this.userId;
        }

        this._eventService.cancelAttendance(this.eventId, userId).then(() => {
            if (userId == this.userId) {
                this.isSignedUp = false;
            }
            this.participants = this.participants.filter((p => p.UserId != userId));
        });
    }


    static create() {
        return () => ({
            template: require('./index.jade'),
            restrict: 'E',
            scope: {},
            replace: false,
            bindToController: true,
            controller: ['eventService', 'parameterService', '$routeParams', EventDetailsController],
            controllerAs: 'controller',
            controllerClass: EventDetailsController
        });
    };

}

module.exports = EventDetailsController;
