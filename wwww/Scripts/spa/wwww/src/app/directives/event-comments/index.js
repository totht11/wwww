'use strict';

class EventCommentsController {

    constructor(eventCommentService) {
        this._eventCommentService = eventCommentService;
    }


    init() {
        this._eventCommentService.reload(this.eventId);
    }


    submitComment() {
        this._eventCommentService.create(this.eventId, this.userId, this.newCommentMessage).then(() => this._eventCommentService.reload(this.eventId));
        this.newCommentMessage = '';
    }


    deleteComment(commentId) {
        this._eventCommentService.delete(this.eventId, commentId).then(() => this._eventCommentService.reload(this.eventId));
    }


    get comments() {
        return this._eventCommentService.list(this.eventId);
    }


    static create() {
        return () => ({
            template: require('./index.jade'),
            restrict: 'E',
            scope: {eventId: "=eventId", userId: "=userId", canDelete: "=canDelete"},
            replace: false,
            bindToController: true,
            controller: ['eventCommentService', EventCommentsController],
            controllerAs: 'controller',
            controllerClass: EventCommentsController
        });
    };

}

module.exports = EventCommentsController;
