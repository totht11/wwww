'use strict';

class UserProfileController {

    constructor($routeParams, userService ) {
        this._$routeParams = $routeParams;
        this._userService = userService;

        this._userService.load();
    }


    get userName() {
        return this._$routeParams.userNickName;
    }


    get userId() {
        return (this._userService.list.filter(c => c.NickName == this.userName)[0] || {Id: null}).Id;
    }


    static create() {
        return () => ({
            template: require('./index.jade'),
            restrict: 'E',
            scope: {},
            replace: false,
            bindToController: true,
            controller: ['$routeParams', 'userService', UserProfileController],
            controllerAs: 'controller',
            controllerClass: UserProfileController
        });
    };

}

module.exports = UserProfileController;
