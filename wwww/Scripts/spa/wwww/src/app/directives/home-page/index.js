'use strict';

class HomePageController {

    constructor($routeParams, userService ) {
    }


    static create() {
        return () => ({
            template: require('./index.jade'),
            restrict: 'E',
            scope: {},
            replace: false,
            bindToController: true,
            controller: ['$routeParams', 'userService', HomePageController],
            controllerAs: 'controller',
            controllerClass: HomePageController
        });
    };

}

module.exports = HomePageController;
