'use strict';

class EventRecommendationsController {

    constructor(eventRecommendationsService) {
        this._eventRecommendationsService = eventRecommendationsService;
        this._eventRecommendationsService.load();
    }


    get events() {
        return this._eventRecommendationsService.list;
    }


    static create() {
        return () => ({
            template: require('./index.jade'),
            restrict: 'E',
            scope: {},
            replace: false,
            bindToController: true,
            controller: ['eventRecommendationsService', EventRecommendationsController],
            controllerAs: 'controller',
            controllerClass: EventRecommendationsController
        });
    };

}

module.exports = EventRecommendationsController;
