'use strict';

class EventCategoryController {

    constructor(eventCategoryService) {
        this._eventCategoryService = eventCategoryService;
        this._eventCategoryService.load();
    }


    get categoryName() {
        return (this._eventCategoryService.list.filter(c => c.Id == this.categoryId)[0] || {Name: ""}).Name;
    }


    static create() {
        return () => ({
            template: require('./index.jade'),
            restrict: 'E',
            scope: {categoryId: "=categoryId"},
            replace: false,
            bindToController: true,
            controller: ['eventCategoryService', EventCategoryController],
            controllerAs: 'controller',
            controllerClass: EventCategoryController
        });
    };

}

module.exports = EventCategoryController;
