'use strict';

class EventListController {

    constructor($scope, eventService) {
        this._eventService = eventService;

        $scope.$watch('controller.userId', () => {
            this._load();
        });
        $scope.$watch('controller.search', () => {
            this._load();
        });
        $scope.$watch('controller.categoryId', () => {
            this._load();
        });

        this.itemsPerPage = 10;
        this.currentPage = 1;
    }


    _load() {
        if (!this.eventsList) {
            this.currentPage = 1;
            this._eventService.loadAll(this._criteria());
        }
    }


    _criteria() {
        let criteria = {};

        if (this.userId) {
            criteria['OwnerUserId'] = this.userId;
        }
        if (this.search) {
            criteria['search'] = this.search;
        }
        if (this.categoryId) {
            criteria['CategoryId'] = this.categoryId;
        }

        return criteria;
    }


    get events () {
        if (this.eventsList) return this._applyPaging(this.eventsList);

        return this._applyPaging(this._eventService.eventsList(this._criteria()) || []);
    }


    _applyPaging(all) {
        return all.slice((this.currentPage - 1) * this.itemsPerPage, this.currentPage * this.itemsPerPage)
    }


    get eventCount () {
        if (this.eventsList) return this.eventsList.length;

        return (this._eventService.eventsList(this._criteria()) || []).length;
    }


    static create() {
        return () => ({
            template: require('./index.jade'),
            restrict: 'E',
            scope: {userId: "=userId", search: "=search", categoryId: "=categoryId", eventsList: "=eventsList"},
            replace: false,
            bindToController: true,
            controller: ['$scope', 'eventService', EventListController],
            controllerAs: 'controller',
            controllerClass: EventListController
        });
    };

}

module.exports = EventListController;
