'use strict';

//var randomstring = require("randomstring");
let randomstring = {
    generate: (length, chars) => {
        if (!length) length = 16;
        if (!chars) chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }
};


class GoogleMapsController {

    constructor($scope, $location) {
        this._$scope = $scope;
        this._$location = $location;

        this.map = null;
        this.mapElementId = randomstring.generate();

        this._markerObjects = [];
    }


    initMap() {
        var mapDiv = document.getElementsByClassName(this.mapElementId)[0];

        if ((typeof google) == 'undefined' || !google || !google.maps || !google.maps.Map || !this.center || !mapDiv || mapDiv.offsetWidth == 0) {
            setTimeout(this.initMap.bind(this), 500);
            return;
        }

        if (this.mapInitialized) return;
        this.mapInitialized = true;

        this.map = new google.maps.Map(mapDiv, {
            center: this.center,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: this.zoom || 10
        });

        this._$scope.$watch('controller.markers', () => {
            this._refreshMarkersOnMap();
        });
        this._refreshMarkersOnMap();
    }


    _refreshMarkersOnMap() {
        // https://developers.google.com/maps/documentation/javascript/examples/marker-remove
        this._markerObjects.forEach((item) => item.setMap(null));
        if (this.markers) {
            this._markerObjects = this.markers.map(this.createMarkerObject.bind(this));
        }
    }


    createMarkerObject(marker) {
        let markerObject = new google.maps.Marker(angular.extend(marker, {map: this.map, animation: google.maps.Animation.DROP}));
        if (marker.url) {
            markerObject.addListener('click', () => {
                this._$location.path(marker.url);
                this._$scope.$apply();
            });
        }
        return markerObject;
    }


    static create() {
        return () => ({
            template: require('./index.jade'),
            restrict: 'E',
            scope: {center: '=center', markers: '=markers', zoom: '=zoom'},
            replace: false,
            bindToController: true,
            controller: ['$scope', '$location', GoogleMapsController],
            controllerAs: 'controller',
            controllerClass: GoogleMapsController
        });
    };

}

module.exports = GoogleMapsController;
