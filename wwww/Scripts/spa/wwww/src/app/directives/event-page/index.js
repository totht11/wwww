'use strict';

class EventPageController {

    constructor($window, geoLocationService, eventService, parameterService, eventCategoryService) {
        this._$window = $window;
        this._geoLocationService = geoLocationService;
        this._eventService = eventService;
        this.userId = parameterService.userId;

        this._eventCategoryService = eventCategoryService;
        this._eventCategoryService.load();

        this.maxDistance = 10;

        this.locationQueryEnded = false;
        this.position = null;
        this.currentCoordinates = null;
        this._startPositionQuery();
        this.map = null;
        this._markersCache = [];
    }


    get markers() {
        var events = this._eventService.eventsList({maxDistance: this.maxDistance * 1000, fromLat: (this.currentCoordinates || {}).lat, fromLon: (this.currentCoordinates || {}).lng});

        if (events && events.length != this._markersCache.length) {
            this._markersCache = events.map((e) => {
                return {position: {lat: e.Lat, lng: e.Lon}, title: e.Title, url: `/events/${e.Id}`};
            });
        }

        return this._markersCache;
    }


    get categoryOptions() {
        return this._eventCategoryService.list;
    }


    reloadNearbyEvents() {
        this._markersCache = [];
        this._loadNearbyEvents();
    }


    _startPositionQuery() {
        this._geoLocationService.getCurrentPosition().then(
            (position) => {
                this.position = position;
                this.currentCoordinates = {
                    lat: this.position.coords.latitude,
                    lng: this.position.coords.longitude
                };
                this.locationQueryEnded = true;
                this._loadNearbyEvents();
            },
            (error) => {
                console.log(error);
                switch (error.code) {
                    case 1:
                        this.locationQueryErrorMessage = 'To see nearby events, please enable this website to access your location.';
                        break;
                    case 2:
                        this.locationQueryErrorMessage = 'Could not find your location.';
                        break;
                    case 3:
                        this.locationQueryErrorMessage = 'Querying your location timed out.';
                        break;
                    default:
                        this.locationQueryErrorMessage = 'Unknown error occurred while querying your location.';
                        break;
                }

                this.locationQueryEnded = true;
            }
        );
    }

    _loadNearbyEvents() {
        this._eventService.loadAll({
            maxDistance: this.maxDistance * 1000,
            fromLat: this.currentCoordinates.lat,
            fromLon: this.currentCoordinates.lng
        });
    }


    static create() {
        return () => ({
            template: require('./index.jade'),
            restrict: 'E',
            scope: {},
            replace: false,
            bindToController: true,
            controller: ['$window', 'geoLocationService', 'eventService', 'parameterService', 'eventCategoryService', EventPageController],
            controllerAs: 'controller',
            controllerClass: EventPageController
        });
    };

}

module.exports = EventPageController;
