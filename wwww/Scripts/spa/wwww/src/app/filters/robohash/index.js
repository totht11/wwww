'use strict';

class RobohashFilter {

  constructor() {
    this.transform = this.transform.bind(this);
  }


  transform(input) {
    return `https://robohash.org/${input}?set=set3&size=50x50`;
  }


  static create() {
    return [() => (new RobohashFilter()).transform];
  }

}

module.exports = RobohashFilter;
