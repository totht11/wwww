'use strict';

let gulp = require('gulp');
let runSequence = require('run-sequence');
let config = require('./tasks.config.js');
let tasks = require('boar-tasks-client').getTasks(gulp, config);

gulp.task('build', function(cb) {
  runSequence('build-clean', 'client-build', cb);
});
gulp.task('start', ['build'], function() {
  gulp.run('assets-watch');
  gulp.run('client-watch');
});
gulp.task('test', tasks.client.test);
gulp.task('code-style', ['client-jscs', 'check-tests', 'check-logs']);
gulp.task('build-clean', tasks.build.clean);
gulp.task('server', tasks.client.staticServer);
gulp.task('client-build', tasks.client.buildScripts);
gulp.task('client-watch', tasks.client.buildScriptsDenyErrors);
gulp.task('reload-static-server', tasks.client.reloadStaticServer);
gulp.task('assets-watch', function() {
  gulp.watch('dist/**/*', ['reload-static-server']);
});

