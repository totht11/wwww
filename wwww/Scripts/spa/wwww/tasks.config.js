'use strict';

let distPath = 'dist/';

module.exports = {
  server: {
    path: 'fake-server/',
    runnable: distPath + 'server.js',
    filePattern: ['fake-server/**/!(*.spec).{html,js}', 'package.json'],
    watchPattern: 'fake-server/**/*.js'
  },
  client: {
    app: {
      buildPattern: 'src/app/app.js',
      target: distPath + 'scripts/',
      loaders: [
        { test: /\.js$/, loader: 'babel', exclude: /(node_modules)/ },
        { test: /\.jade$/, loader: 'jade-loader?self' },
        { test: /\.json$/, loader: 'json-loader' }
      ]
    }
  }
};
