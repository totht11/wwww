﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace wwww
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // need explicit routes for this one, as the webapi attribute routing does not support intercontroller priority setting
            config.Routes.MapHttpRoute(
                name: "UsersApi",
                routeTemplate: "api/Users",
                defaults: new { controller = "Users" }
            );

            config.MapHttpAttributeRoutes();
        }
    }
}
