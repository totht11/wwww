﻿using Microsoft.Owin;
using Microsoft.Owin.Extensions;
using Owin;
using wwww.LoginTokenAuthentication;
using WwwwLogger;

[assembly: OwinStartupAttribute(typeof(wwww.Startup))]

namespace wwww
{

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Use(new LoggingMiddleware());

            ConfigureAuth(app);

            app.Use(new LoginTokenAuthenticationMiddleware());
            app.UseStageMarker(PipelineStage.Authenticate);
        }
    }

}
