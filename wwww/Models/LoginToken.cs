﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace wwww.Models
{
    public class LoginToken
    {
        public int Id { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [MaxLength(64)]
        public string Token { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public DateTime Created { get; set; }

    }
}
