﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscherClient;
using HistoryServiceClient.DTO;
using Newtonsoft.Json;
using WwwwLogger;

namespace HistoryServiceClient
{
    public class ActionHistoryClient
    {

        const string ActionHistoryResource = "http://localhost:10003/api/ActionHistory";

        private readonly EscherHttpClient client = new EscherHttpClient();


        public async Task StoreActionAsync(EventCreationAction eventCreationAction, CancellationToken cancellationToken)
        {
            await PostAction(cancellationToken, "/EventCreation", JsonConvert.SerializeObject(eventCreationAction));
        }


        public async Task StoreActionAsync(EventSubscriptionAction eventSubscriptionAction, CancellationToken cancellationToken)
        {
            await PostAction(cancellationToken, "/EventSubscription", JsonConvert.SerializeObject(eventSubscriptionAction));
        }


        public async Task StoreActionAsync(SearchByCategoryAction searchByCategoryAction, CancellationToken cancellationToken)
        {
            await PostAction(cancellationToken, "/SearchByCategory", JsonConvert.SerializeObject(searchByCategoryAction));
        }


        private async Task PostAction(CancellationToken cancellationToken, string eventCreationPath, string postData)
        {
            var content = new StringContent(postData, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(ActionHistoryResource + eventCreationPath, content, cancellationToken);

            var statusCode = (int) response.StatusCode;
            if (statusCode >= 400)
            {
                Logger.Error(Logger.Message("ActionHistory cannot be saved")
                    .AddParameter("ResponseCode", response.StatusCode)
                    .AddParameter("RequestBody", postData)
                    .AddParameter("ResponseBody", await response.Content.ReadAsStringAsync())
                );
            }

            // fail silently, it is not mission critical
        }

    }
}
