﻿using System;

namespace HistoryServiceClient.DTO
{
    public class EventCreationAction
    {
        public string UserId { get; set; }

        public int EventId { get; set; }

        public int EventCategoryId { get; set; }
    }
}
