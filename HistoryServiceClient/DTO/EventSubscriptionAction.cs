﻿using System;

namespace HistoryServiceClient.DTO
{
    public class EventSubscriptionAction
    {
        public string UserId { get; set; }

        public int EventId { get; set; }

        public int EventCategoryId { get; set; }
    }
}
