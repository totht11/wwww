﻿using System;

namespace HistoryServiceClient.DTO
{
    public class SearchByCategoryAction
    {
        public string UserId { get; set; }

        public int EventCategoryId { get; set; }
    }
}
