﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;

namespace WwwwLogger
{
    public static class Logger
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public static void Debug(LogParameter logParameter)
        {
            logger.Debug(logParameter.ToString());
        }

        public static void Info(LogParameter logParameter)
        {
            logger.Info(logParameter.ToString());
        }

        public static void Warn(LogParameter logParameter)
        {
            logger.Warn(logParameter.ToString());
        }

        public static void Error(LogParameter logParameter)
        {
            logger.Error(logParameter.ToString());
        }

        public static void Fatal(LogParameter logParameter)
        {
            logger.Fatal(logParameter.ToString());
        }

        public static LogParameter Message(string message = "", [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            return new LogParameter()
                .SetMessage(message)
                .AddParameter("CallerMemberName", memberName)
                .AddParameter("CallerFilePath", sourceFilePath + ":" + sourceLineNumber);
        }


        public class LogParameter
        {
            private readonly Dictionary<string, object> data = new Dictionary<string, object>();

            public LogParameter AddParameter(string key, object value)
            {
                this.data.Add(key, value);
                return this;
            }

            public LogParameter SetMessage(string message)
            {
                return this.AddParameter("Message", message);
            }

            public LogParameter SetException(Exception exception)
            {
                this.SetExceptionWithPrefix(exception);
                return this;
            }

            private void SetExceptionWithPrefix(Exception exception, string prefix = "")
            {
                this.data.Add(prefix + "ExceptionMessage", exception.Message);
                this.data.Add(prefix + "ExceptionSource", exception.Source);
                this.data.Add(prefix + "ExceptionTargetSite", exception.TargetSite.ToString());

                if (exception.InnerException != null)
                {
                    this.SetExceptionWithPrefix(exception, "Inner" + prefix);
                }
            }

            public override string ToString()
            {
                return JsonConvert.SerializeObject(this.data, Formatting.Indented);
            }
        }
    }
}
