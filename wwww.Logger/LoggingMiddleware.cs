﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using AppFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;


namespace WwwwLogger
{
    public class LoggingMiddleware
    {
        private AppFunc next;

        public void Initialize(AppFunc next)
        {
            this.next = next;
        }

        public async Task Invoke(IDictionary<string, object> environment)
        {
            var stopwatch = Stopwatch.StartNew();
            await next.Invoke(environment);
            stopwatch.Stop();

            var message = Logger.Message("Requestlog")
                .AddParameter("owin.RequestMethod", environment["owin.RequestMethod"])
                .AddParameter("owin.RequestPath", environment["owin.RequestPath"])
                .AddParameter("owin.RequestPathBase", environment["owin.RequestPathBase"])
                .AddParameter("owin.RequestQueryString", environment["owin.RequestQueryString"])
                .AddParameter("owin.RequestScheme", environment["owin.RequestScheme"])
                .AddParameter("owin.ResponseStatusCode", environment["owin.ResponseStatusCode"])
                .AddParameter("durationInMilliseconds", stopwatch.ElapsedMilliseconds);

            var responseCode = (int?) environment["owin.ResponseStatusCode"];

            if (responseCode >= 500)
            {
                Logger.Error(message);
            }
            else if (responseCode >= 400)
            {
                Logger.Warn(message);
            }
            else if (responseCode >= 200)
            {
                Logger.Debug(message);
            }
            else
            {
                Logger.Error(message);
            }
        }
    }
}
