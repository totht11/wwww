﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HistoryService.DTO
{
    public class CategoryStat
    {
        public int CategoryId { get; set; }

        public int Count { get; set; } = 0;

    }
}
