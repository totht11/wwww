﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HistoryService.Models
{
    public class EventCreationAction
    {
        public int Id { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public int EventId { get; set; }

        [Required]
        public int EventCategoryId { get; set; }

        public DateTime Created { get; set; }
    }
}
