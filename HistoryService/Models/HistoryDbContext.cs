﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HistoryService.Models
{
    public class HistoryDbContext : DbContext
    {
        public HistoryDbContext()
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<EventCreationAction> EventCreationActions { get; set; }
        public DbSet<EventSubscriptionAction> EventSubscriptionActions { get; set; }
        public DbSet<SearchByCategoryAction> SearchByCategoryActions { get; set; }
    }
}
