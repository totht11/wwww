﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using HistoryService.DTO;
using HistoryService.Models;

namespace HistoryService.Controllers
{
    [Authorize]
    public class ActionHistoryController : ApiController
    {
        private readonly HistoryDbContext db = new HistoryDbContext();

        [HttpGet]
        [Route("api/Users/{userId}/PreferredCategories")]
        [ResponseType(typeof(IQueryable<CategoryStat>))]
        public IHttpActionResult GetPreferredCategories(string userId)
        {
            var eventCreationCategoryStats =
                db.EventCreationActions.Where(eca => eca.UserId == userId).Select(eca => eca.EventCategoryId);

            var eventSubscriptionCategoryStats =
                db.EventSubscriptionActions.Where(eca => eca.UserId == userId).Select(eca => eca.EventCategoryId);

            var eventSearchCategoryStats =
                db.SearchByCategoryActions.Where(eca => eca.UserId == userId).Select(eca => eca.EventCategoryId);


            var stats = eventCreationCategoryStats.Concat(eventSubscriptionCategoryStats.Concat(eventSearchCategoryStats))
                .GroupBy(categoryId => categoryId).Select(group => new CategoryStat { CategoryId = group.Key, Count = group.Count() });
            
            return Ok(stats);
        }


        [HttpPost]
        [Route("api/ActionHistory/EventCreation")]
        [ResponseType(typeof(EventCreationAction))]
        public async Task<IHttpActionResult> PostEventCreationAction(EventCreationAction eventCreationAction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            eventCreationAction.Created = DateTime.Now;

            db.EventCreationActions.Add(eventCreationAction);
            await db.SaveChangesAsync();

            return Content(HttpStatusCode.Created, eventCreationAction);
        }


        [HttpPost]
        [Route("api/ActionHistory/EventSubscription")]
        [ResponseType(typeof(EventSubscriptionAction))]
        public async Task<IHttpActionResult> PostEventSubscriptionAction(EventSubscriptionAction eventSubscriptionAction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            eventSubscriptionAction.Created = DateTime.Now;

            db.EventSubscriptionActions.Add(eventSubscriptionAction);
            await db.SaveChangesAsync();

            return Content(HttpStatusCode.Created, eventSubscriptionAction);
        }


        [HttpPost]
        [Route("api/ActionHistory/SearchByCategory")]
        [ResponseType(typeof(SearchByCategoryAction))]
        public async Task<IHttpActionResult> PostSearchByCategoryAction(SearchByCategoryAction searchByCategoryAction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            searchByCategoryAction.Created = DateTime.Now;

            db.SearchByCategoryActions.Add(searchByCategoryAction);
            await db.SaveChangesAsync();

            return Content(HttpStatusCode.Created, searchByCategoryAction);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
