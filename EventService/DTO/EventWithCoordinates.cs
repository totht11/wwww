﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventService.DTO
{
    public class EventWithCoordinates : Event
    {
        public double Lat { get; set; }
        public double Lon { get; set; }
    }
}