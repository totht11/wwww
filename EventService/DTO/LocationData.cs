﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventService.DTO
{
    public class LocationData
    {
        public int Id { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
    }
}
