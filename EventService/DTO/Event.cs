﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using EventService.Models;

namespace EventService.DTO
{
    public class Event
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(128)]
        public string Title { get; set; }

        public string OwnerUserId { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        public DateTime StartTime { get; set; }

        [Required]
        public long Duration { get; set; }

        [Required]
        public DurationUnit DurationUnit { get; set; }

        [Required]
        [MaxLength(128)]
        public string Country { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        [Required]
        [MaxLength(128)]
        public string LocationName { get; set; }

        [Required]
        [MaxLength(128)]
        public string City { get; set; }

        [Required]
        [MaxLength(256)]
        public string Address { get; set; }

        public DateTime Created { get; set; }

        public byte[] TimeStamp { get; set; }
    }

}
