﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventService.Models
{
    public class Comment
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(2048)]
        public string Message { get; set; }

        [Required]
        [MaxLength(128)]
        public string UserId { get; set; }

        [Required]
        [Index]
        public int EventId { get; set; }

        public Event Event { get; set; }

        public DateTime Created { get; set; }
    }
}