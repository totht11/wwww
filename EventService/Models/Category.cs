﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventService.Models
{
    public class Category
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(128)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        public List<Event> Events { get; set; }
    }
}
