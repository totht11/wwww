﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventService.Models
{
    public class Event
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(128)]
        public string Title { get; set; }
        
        [Required]
        [MaxLength(128)]
        [Index]
        public string OwnerUserId { get; set; }

        public int CategoryId { get; set; }

        [Required]
        public DateTime StartTime { get; set; }

        [Required]
        public long Duration { get; set; }

        [Required]
        public DurationUnit DurationUnit { get; set; }

        [Required]
        [MaxLength(128)]
        public string Country { get; set; }

        [Required]
        [MaxLength(128)]
        public string City { get; set; }

        [Required]
        [MaxLength(256)]
        public string Address { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        [MaxLength(128)]
        public string LocationName { get; set; }

        public int LocationId { get; set; }

        [Timestamp]
        public byte[] TimeStamp { get; set; }
        
        public DateTime Created { get; set; }

        public Category Category { get; set; }

        public EventSubscription EventSubscriptions { get; set; }

    }

    public enum DurationUnit
    {
        Seconds = 1,
        Minutes = 2,
        Hours = 3
    }

}
