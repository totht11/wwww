﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventService.Models
{
    public class EventSubscription
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(128)]
        [Index]
        public string UserId { get; set; }

        [Required]
        [Index]
        public int EventId { get; set; }

        public bool IsKicked { get; set; } = false;

        public DateTime Created { get; set; }

        [Timestamp]
        public byte[] TimeStamp { get; set; }

    }
}
