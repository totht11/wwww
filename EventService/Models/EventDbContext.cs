﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EventService.Models
{
    public class EventDbContext : DbContext
    {
        public EventDbContext()
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Event> Events { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<EventSubscription> EventSubscriptions { get; set; }

        public DbSet<Comment> Comments { get; set; }
    }
}
