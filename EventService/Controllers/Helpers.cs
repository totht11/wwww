﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace EventService.Controllers
{
    static class Helpers
    {
        public static string GetUserIdFromHeaders(this ApiController controller, bool failIfNotFound = true)
        {
            var header = controller.Request.Headers.SingleOrDefault(h => String.Equals(h.Key, "X-WWWW-UserId", StringComparison.OrdinalIgnoreCase));

            if (header.Value != null)
            {
                return header.Value.Single();
            }

            if (failIfNotFound)
            {
                throw new InvalidOperationException("no user-id header");
            }

            return null;
        }
    }
}
