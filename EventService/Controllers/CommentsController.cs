﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EventService.Models;

namespace EventService.Controllers
{
    [Authorize]
    public class CommentsController : ApiController
    {
        private readonly EventDbContext db = new EventDbContext();


        [HttpGet]
        [Route("api/Events/{eventId}/Comments")]
        public async Task<IHttpActionResult> GetComments(int eventId, CancellationToken cancellationToken)
        {
            var @event = await db.Events.FindAsync(cancellationToken, eventId);
            if (@event == null)
            {
                return NotFound();
            }

            var currentUserId = this.GetUserIdFromHeaders();
            if (@event.OwnerUserId != currentUserId && (await db.EventSubscriptions.FirstOrDefaultAsync(es => es.EventId == eventId && es.UserId == currentUserId, cancellationToken)) == null)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            return Ok(db.Comments.Where(c => c.EventId == eventId).OrderByDescending(c => c.Id));
        }


        [HttpPost]
        [Route("api/Events/{eventId}/Comments")]
        public async Task<IHttpActionResult> PostComment(Comment comment, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var @event = await db.Events.FindAsync(cancellationToken, comment.EventId);
            if (@event == null)
            {
                return NotFound();
            }

            comment.Created = DateTime.Now;
            db.Comments.Add(comment);
            await db.SaveChangesAsync(cancellationToken);

            return StatusCode(HttpStatusCode.Created);
        }


        [HttpDelete]
        [Route("api/Events/{eventId}/Comments/{id}")]
        public async Task<IHttpActionResult> DeleteComment(int id, CancellationToken cancellationToken)
        {
            var comment = await db.Comments.Where(c => c.Id == id).Include(c => c.Event).SingleOrDefaultAsync(cancellationToken);
            if (comment == null)
            {
                return NotFound();
            }

            if (comment.Event.OwnerUserId != this.GetUserIdFromHeaders())
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            db.Comments.Remove(comment);
            await db.SaveChangesAsync(cancellationToken);

            return Ok(comment);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}