﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using EventService.Models;
using HistoryServiceClient;
using HistoryServiceClient.DTO;

namespace EventService.Controllers
{
    [Authorize]
    public class EventSubscriptionsController : ApiController
    {

        private readonly EventDbContext db = new EventDbContext();
        private readonly ActionHistoryClient actionHistoryClient = new ActionHistoryClient();


        [HttpGet]
        [Route("api/Events/{eventId}/Subscriptions")]
        public async Task<IEnumerable<EventSubscription>> GetEventSubscriptions(int eventId, CancellationToken cancellationToken, string UserId = null)
        {
            if (UserId == null)
            {
                return await db.EventSubscriptions.Where(es => es.EventId == eventId).ToListAsync(cancellationToken);
            }

            return await db.EventSubscriptions.Where(es => es.EventId == eventId && es.UserId == UserId).ToListAsync(cancellationToken);
        }


        [HttpGet]
        [Route("api/Events/Subscriptions")]
        public IQueryable<EventSubscription> GetEventSubscriptions()
        {
            var userId = this.GetUserIdFromHeaders();
            return db.EventSubscriptions.Where(es => es.UserId == userId);
        }


        [HttpPost]
        [Route("api/Events/{eventId}/Subscriptions")]
        [ResponseType(typeof(EventSubscription))]
        public async Task<IHttpActionResult> PostEventSubscription(int eventId, CancellationToken cancellationToken)
        {
            var @event = await db.Events.SingleAsync(e => e.Id == eventId, cancellationToken);

            await SaveEventCreationAction(@event, cancellationToken);

            var eventSubscription = new EventSubscription
            {
                Created = DateTime.Now,
                EventId = eventId,
                UserId = this.GetUserIdFromHeaders()
            };

            Validate(eventSubscription);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EventSubscriptions.Add(eventSubscription);
            await db.SaveChangesAsync(cancellationToken);

            return StatusCode(HttpStatusCode.Created);
        }


        [HttpDelete]
        [Route("api/Events/{eventId}/Subscriptions")]
        [ResponseType(typeof(EventSubscription))]
        public async Task<IHttpActionResult> DeleteEventSubscription(int eventId, string UserId = null)
        {
            IEnumerable<EventSubscription> eventSubscription;
            var currentUserId = this.GetUserIdFromHeaders();

            if (UserId == null)
            {
                // unsubscribe
                eventSubscription = db.EventSubscriptions.Where(es => es.EventId == eventId && es.UserId == currentUserId);
            }
            else
            {
                // kick
                if ((!await db.Events.AnyAsync(e => e.Id == eventId && e.OwnerUserId == currentUserId)) && UserId != currentUserId)
                {
                    throw new HttpResponseException(HttpStatusCode.Forbidden);
                }

                eventSubscription = db.EventSubscriptions.Where(es => es.EventId == eventId && es.UserId == UserId);
            }

            if (!eventSubscription.Any())
            {
                return NotFound();
            }

            db.EventSubscriptions.RemoveRange(eventSubscription);
            await db.SaveChangesAsync();

            return Ok(eventSubscription);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        private async Task SaveEventCreationAction(Event eventModel, CancellationToken cancellationToken)
        {
            var userIdFromHeaders = this.GetUserIdFromHeaders(false);
            if (userIdFromHeaders != null)
            {
                await actionHistoryClient.StoreActionAsync(
                        new EventSubscriptionAction()
                        {
                            EventCategoryId = eventModel.CategoryId,
                            EventId = eventModel.Id,
                            UserId = userIdFromHeaders
                        }, cancellationToken);
            }
        }
    }
}
