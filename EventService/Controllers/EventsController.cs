﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using System.Web.Http.Filters;
using EscherClient;
using EventService.DTO;
using EventService.Models;
using HistoryServiceClient;
using HistoryServiceClient.DTO;
using Newtonsoft.Json;
using WwwwLogger;
using Event = EventService.Models.Event;

namespace EventService.Controllers
{
    [Authorize]
    public class EventsController : ApiController
    {

        private readonly EventDbContext db = new EventDbContext();
        private readonly EscherHttpClient httpClient = new EscherHttpClient();
        private readonly ActionHistoryClient actionHistoryClient = new ActionHistoryClient();


        // GET: api/Events
        public async Task<IHttpActionResult> GetEvents(CancellationToken cancellationToken, string OwnerUserId = null, string search = null, int? CategoryId = null, int? limit = null, bool withCoords = false)
        {
            IQueryable<Event> result = db.Events;

            if (OwnerUserId != null)
            {
                result = result.Where(e => e.OwnerUserId == OwnerUserId);
            }
            if (search != null)
            {
                result = result.Where(e => e.Title.ToLower().Contains(search.ToLower()));
            }
            if (CategoryId != null)
            {
                result = result.Where(e => e.CategoryId == CategoryId);
                await SaveSearchByCategoryAction(CategoryId.Value, cancellationToken);
            }
            if (limit != null)
            {
                result = result.Take(limit.Value);
            }

            result = result.OrderByDescending(e => e.Created);

            if (!withCoords)
            {
                return Ok(result.Select(e => new EventService.DTO.Event
                {
                    Id = e.Id,
                    Title = e.Title,
                    OwnerUserId = e.OwnerUserId,
                    CategoryId = e.CategoryId,
                    Created = e.Created,
                    Duration = e.Duration,
                    DurationUnit = e.DurationUnit,
                    StartTime = e.StartTime,
                    TimeStamp = e.TimeStamp,
                    Country = e.Country,
                    Description = e.Description,
                    LocationName = e.LocationName,
                    City = e.City,
                    Address = e.Address
                }));
            }

            var eventsWithLocation = (await result.ToArrayAsync(cancellationToken))
                .Select(async e => await EventModelToEventWithCoordinates(e, cancellationToken));

            return Ok(await Task.WhenAll(eventsWithLocation));
        }

        // GET: api/Events/5
        [ResponseType(typeof(EventWithCoordinates))]
        public async Task<IHttpActionResult> GetEvent(int id, CancellationToken cancellationToken)
        {
            var eventModel = await db.Events.FindAsync(cancellationToken, id);
            if (eventModel == null)
            {
                return NotFound();
            }

            return Ok(await EventModelToEventWithCoordinates(eventModel, cancellationToken));
        }

        private async Task<EventWithCoordinates> EventModelToEventWithCoordinates(Event eventModel, CancellationToken cancellationToken)
        {
            var locationData = await LoadLocationData(cancellationToken, eventModel);

            return new EventService.DTO.EventWithCoordinates()
            {
                Id = eventModel.Id,
                Title = eventModel.Title,
                OwnerUserId = eventModel.OwnerUserId,
                CategoryId = eventModel.CategoryId,
                Created = eventModel.Created,
                Duration = eventModel.Duration,
                DurationUnit = eventModel.DurationUnit,
                StartTime = eventModel.StartTime,
                TimeStamp = eventModel.TimeStamp,
                Country = eventModel.Country,
                Description = eventModel.Description,
                LocationName = eventModel.LocationName,
                City = eventModel.City,
                Address = eventModel.Address,
                Lat = locationData.Lat,
                Lon = locationData.Lon
            };
        }


        private async Task<LocationData> LoadLocationData(CancellationToken cancellationToken, Event eventModel)
        {
            var response = await httpClient.GetAsync("http://localhost:10002/api/Locations/" + eventModel.LocationId, cancellationToken);

            if (response.StatusCode >= HttpStatusCode.InternalServerError)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            if (response.StatusCode >= HttpStatusCode.BadRequest)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, await response.Content.ReadAsStringAsync()));
            }

            return JsonConvert.DeserializeObject<LocationData>(await response.Content.ReadAsStringAsync());
        }


        // GET: api/Events
        [ResponseType(typeof(IQueryable<EventWithCoordinates>))]
        public async Task<IHttpActionResult> GetEvent(double maxDistance, double fromLat, double fromLon, CancellationToken cancellationToken)
        {
            var response = await httpClient.GetAsync("http://localhost:10002/api/Locations?maxDistance=" + HttpUtility.UrlEncode(maxDistance.ToString(CultureInfo.InvariantCulture))
                + "&fromLat=" + HttpUtility.UrlEncode(fromLat.ToString(CultureInfo.InvariantCulture)) + "&fromLon=" + HttpUtility.UrlEncode(fromLon.ToString(CultureInfo.InvariantCulture)), cancellationToken);
            if (response.StatusCode >= HttpStatusCode.InternalServerError)
            {
                return InternalServerError();
            }
            if (response.StatusCode >= HttpStatusCode.BadRequest)
            {
                return BadRequest(await response.Content.ReadAsStringAsync());
            }
            var locationData = JsonConvert.DeserializeAnonymousType(await response.Content.ReadAsStringAsync(), new[] { new { Id = 0, Lat = 0.0, Lon = 0.0 } });
            var locationIds = locationData.Select(l => l.Id);

            return Ok((await db.Events.Where(e => locationIds.Contains(e.LocationId)).ToListAsync(cancellationToken)).Select(e => new EventService.DTO.EventWithCoordinates()
            {
                Id = e.Id,
                Title = e.Title,
                OwnerUserId = e.OwnerUserId,
                CategoryId = e.CategoryId,
                Created = e.Created,
                Duration = e.Duration,
                DurationUnit = e.DurationUnit,
                StartTime = e.StartTime,
                TimeStamp = e.TimeStamp,
                Country = e.Country,
                Description = e.Description,
                LocationName = e.LocationName,
                City = e.City,
                Address = e.Address,
                Lat = locationData.Single(l => l.Id == e.LocationId).Lat,
                Lon = locationData.Single(l => l.Id == e.LocationId).Lon
            }));
        }

        // POST: api/Events
        [ResponseType(typeof(EventService.DTO.Event))]
        public async Task<IHttpActionResult> PostEvent(EventService.DTO.Event @event, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var eventModel = new Event()
            {
                OwnerUserId = this.GetUserIdFromHeaders(),
                Duration = @event.Duration,
                DurationUnit = @event.DurationUnit,
                StartTime = @event.StartTime,
                Title = @event.Title,
                CategoryId = @event.CategoryId,
                Created = DateTime.Now,
                Country = @event.Country,
                Description = @event.Description,
                LocationName = @event.LocationName,
                City = @event.City,
                Address = @event.Address
            };

            Validate(eventModel);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var locationId = await RegisterLocation(cancellationToken, eventModel);
                eventModel.LocationId = locationId;

                db.Events.Add(eventModel);

                await db.SaveChangesAsync(cancellationToken);

                await SaveEventCreationAction(eventModel, cancellationToken);
            }
            catch (Exception)
            {
                if (eventModel.Id > 0)
                {
                    db.Events.Remove(eventModel);
                    await db.SaveChangesAsync();
                }

                throw;
            }

            return CreatedAtRoute("DefaultApi", new { id = eventModel.Id }, eventModel);
        }

        private async Task<int> RegisterLocation(CancellationToken cancellationToken, Event eventModel)
        {
            var postData = JsonConvert.SerializeObject(new { Address = string.Format("{0} {1} {2}", eventModel.Country, eventModel.City, eventModel.Address) });
            var response = await httpClient.PostAsync("http://localhost:10002/api/Locations", new StringContent(postData, Encoding.UTF8, "application/json"), cancellationToken);

            if (response.StatusCode >= HttpStatusCode.InternalServerError)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            if (response.StatusCode >= HttpStatusCode.BadRequest)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, await response.Content.ReadAsStringAsync()));
            }

            var responseData = JsonConvert.DeserializeAnonymousType(await response.Content.ReadAsStringAsync(), new { Id = 0 });

            return responseData.Id;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        private async Task SaveEventCreationAction(Event eventModel, CancellationToken cancellationToken)
        {
            var userIdFromHeaders = this.GetUserIdFromHeaders(false);
            if (userIdFromHeaders != null)
            {
                await actionHistoryClient.StoreActionAsync(
                        new EventCreationAction()
                        {
                            EventCategoryId = eventModel.CategoryId,
                            EventId = eventModel.Id,
                            UserId = userIdFromHeaders
                        }, cancellationToken);
            }
        }


        private async Task SaveSearchByCategoryAction(int categoryId, CancellationToken cancellationToken)
        {
            var userIdFromHeaders = this.GetUserIdFromHeaders(false);
            if (userIdFromHeaders != null)
            {
                await actionHistoryClient.StoreActionAsync(
                        new SearchByCategoryAction()
                        {
                            EventCategoryId = categoryId,
                            UserId = userIdFromHeaders
                        }, cancellationToken);
            }
        }
    }
}
