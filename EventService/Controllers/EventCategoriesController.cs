﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EventService.Models;

namespace EventService.Controllers
{
    [Authorize]
    public class EventCategoriesController : ApiController
    {
        private readonly EventDbContext db = new EventDbContext();

        [HttpGet]
        [Route("api/Events/Categories")]
        public IQueryable<Category> GetCategories()
        {
            return db.Categories;
        }

        [HttpGet]
        [Route("api/Events/Categories/{id}")]
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> GetCategory(int id)
        {
            Category category = await db.Categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            return Ok(category);
        }
        
        [HttpPost]
        [Route("api/Events/Categories")]
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> PostCategory(Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Categories.Add(category);
            await db.SaveChangesAsync();

            return CreatedAtRoute("GetEventCategoryById", new { id = category.Id }, category);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
