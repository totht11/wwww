﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using GoogleMapsApi;
using GoogleMapsApi.Entities.Geocoding.Request;
using LocationService.Models;
using WwwwLogger;
using Status = GoogleMapsApi.Entities.Geocoding.Response.Status;
using System.Data.Entity.SqlServer;

namespace LocationService.Controllers
{
    [Authorize]
    public class LocationController : ApiController
    {

        private readonly LocationDbContext db = new LocationDbContext();


        [HttpGet]
        [Route("api/Locations/{id}", Name = "GetLocationById")]
        public async Task<IHttpActionResult> GetLocation(int id)
        {
            var location = await db.Locations.FindAsync(id);

            if (location == null)
            {
                return NotFound();
            }

            return Ok(location);
        }


        [HttpGet]
        [Route("api/Locations", Name = "GetLocationInRange")]
        public IHttpActionResult GetLocationInRange(double maxDistance, double fromLat, double fromLon)
        {
            // =ACOS( SIN(lat1)*SIN(lat2) + COS(lat1)*COS(lat2)*COS(lon2-lon1) ) * 6371000
            // http://www.movable-type.co.uk/scripts/latlong.html
            var locationInDistance = db.Locations.Where(l =>
                SqlFunctions.Acos(
                    SqlFunctions.Sin(SqlFunctions.Radians(fromLat))
                        * SqlFunctions.Sin(SqlFunctions.Radians(l.Lat)) +
                    SqlFunctions.Cos(SqlFunctions.Radians(fromLat))
                        * SqlFunctions.Cos(SqlFunctions.Radians(l.Lat))
                        * SqlFunctions.Cos(SqlFunctions.Radians(l.Lon) - SqlFunctions.Radians(fromLon))
                ) < (maxDistance / 6371000)
            );

            return Ok(locationInDistance);
        }


        [HttpPost]
        [Route("api/Locations")]
        [ResponseType(typeof(Location))]
        public async Task<IHttpActionResult> PostLocation(Location location)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var geocodingResponse = GoogleMaps.Geocode.Query(new GeocodingRequest
            {
                ApiKey = "AIzaSyA2Hg8DmLkia6UgYngBB-gakpN3KENgHKg",
                Address = location.Address
            });

            switch (geocodingResponse.Status)
            {
                case Status.OK:
                    Logger.Debug(Logger.Message("GoogleMapsApi geocoding result").AddParameter("api_response", geocodingResponse));
                    break;
                case Status.OVER_QUERY_LIMIT:
                    Logger.Error(Logger.Message("GoogleMapsApi query limit exceeded").AddParameter("api_response", geocodingResponse));
                    return InternalServerError();
                case Status.ZERO_RESULTS:
                    return BadRequest("No such place.");
                default:
                    Logger.Error(Logger.Message("GoogleMapsApi unexpected response").AddParameter("api_response", geocodingResponse));
                    return InternalServerError();
            }

            location.Created = DateTime.Now;
            location.Lat = geocodingResponse.Results.First().Geometry.Location.Latitude;
            location.Lon = geocodingResponse.Results.First().Geometry.Location.Longitude;
            location.SinLat = Math.Sin(location.Lat.ToRadians());
            location.CosLat = Math.Cos(location.Lat.ToRadians());

            db.Locations.Add(location);
            await db.SaveChangesAsync();

            return CreatedAtRoute("GetLocationById", new { id = location.Id }, location);
        }

    }

    public static class NumericExtensions
    {
        public static double ToRadians(this double val)
        {
            return (Math.PI / 180) * val;
        }
    }

}
