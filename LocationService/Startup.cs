﻿using System;
using System.Threading.Tasks;
using EscherAuth;
using EscherAuth.Middlewares.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Extensions;
using Owin;
using WwwwLogger;

[assembly: OwinStartup(typeof(LocationService.Startup))]

namespace LocationService
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Use(new LoggingMiddleware());

            app.Use(typeof(EscherMiddleware), new KeyDb(), null, (Action<string>)(s => Logger.Debug(Logger.Message(s))));
            app.UseStageMarker(PipelineStage.Authenticate);
        }
    }

    class KeyDb : IKeyDb
    {
        public string this[string key] => "secret";
    }
}
