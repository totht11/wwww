﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LocationService.Models
{
    public class LocationDbContext : DbContext
    {
        public LocationDbContext()
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Location> Locations { get; set; }

    }
}
