﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocationService.Models
{
    public class Location
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(512)]
        public string Address { get; set; }

        public double Lat { get; set; }
        public double Lon { get; set; }

        // denormalized for performance benefits - must be calculated on insert
        public double SinLat { get; set; }
        public double CosLat { get; set; }

        [Timestamp]
        public byte[] TimeStamp { get; set; }
        
        public DateTime Created { get; set; }

    }

}
