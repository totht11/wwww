﻿using System.IO;
using System.Web.Http;
using System.Web.Mvc;
using WwwwLogger;

namespace LocationService
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));

            Logger.Info(Logger.Message("EventService.WebApiApplication.Application_Start called"));

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        }
    }
}
