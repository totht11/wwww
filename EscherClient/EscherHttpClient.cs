﻿using EscherAuth;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscherClient
{
    public class EscherHttpClient
    {

        private const int RetryCount = 3;

        private readonly HttpClient httpClient = new HttpClient();
        private readonly Escher escher = new Escher();


        public EscherHttpClient()
        {

        }


        public async Task<HttpResponseMessage> GetAsync(string requestUri, CancellationToken cancellationToken)
        {
            return await SendAsync(CreateRequest(requestUri, HttpMethod.Get), cancellationToken);
        }


        public async Task<HttpResponseMessage> PostAsync(string requestUri, HttpContent content, CancellationToken cancellationToken)
        {
            return await SendAsync(CreateRequest(requestUri, HttpMethod.Post, content), cancellationToken);
        }


        public async Task<HttpResponseMessage> PutAsync(string requestUri, HttpContent content, CancellationToken cancellationToken)
        {
            return await SendAsync(CreateRequest(requestUri, HttpMethod.Put, content), cancellationToken);
        }


        public async Task<HttpResponseMessage> PatchAsync(string requestUri, HttpContent content, CancellationToken cancellationToken)
        {

            return await SendAsync(CreateRequest(requestUri, new HttpMethod("PATCH"), content), cancellationToken);
        }


        public async Task<HttpResponseMessage> DeleteAsync(string requestUri, CancellationToken cancellationToken)
        {
            return await SendAsync(CreateRequest(requestUri, HttpMethod.Delete), cancellationToken);
        }


        private HttpRequestMessage CreateRequest(string requestUri, HttpMethod method, HttpContent content = null)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(requestUri),
                Method = method
            };

            if (content != null)
            {
                request.Content = content;
            }

            return request;
        }


        public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return await SendAsync(new HttpRequestMessageAdapter(request), cancellationToken);
        }


        private async Task<HttpResponseMessage> SendAsync(HttpRequestMessageAdapter request, CancellationToken cancellationToken)
        {
            HttpResponseMessage response = null;

            int counter = 0;
            bool failed = false;
            do
            {
                await Task.Delay(counter * 1000, cancellationToken);

                try
                {
                    if (failed)
                    {
                        request = request.Clone();
                    }

                    response = await httpClient.SendAsync(escher.SignRequest(request, "key", "secret"), cancellationToken);
                    failed = response.StatusCode >= HttpStatusCode.InternalServerError;

                    if (failed)
                    {
                        Debug.WriteLine("failed for the {0}th time", counter);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine("failed for the {0}th time", counter);
                    Debug.WriteLine(e.Message);
                    if ( counter < RetryCount)
                    {
                        failed = true;
                    }
                    else
                    {
                        throw;
                    }
                }

            } while (counter++ < RetryCount && failed);

            return response;
        }

    }
}
