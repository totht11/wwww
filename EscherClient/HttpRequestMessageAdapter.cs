﻿using EscherAuth.Request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EscherClient
{
    internal class HttpRequestMessageAdapter : IEscherRequest
    {
        private readonly HttpRequestMessage request;

        private string bodyCache = null;

        public string Method => request.Method.Method;
        public Uri Uri => request.RequestUri;
        public List<Header> Headers { get; }
        public string Body
        {
            get
            {
                if (bodyCache == null)
                {
                    bodyCache = request.Content?.ReadAsStringAsync().Result ?? "";
                }

                return bodyCache;
            }
        }

        public HttpRequestMessageAdapter(HttpRequestMessage request)
        {
            this.request = request;
            this.Headers = request.Headers.SelectMany(h => h.Value.Select(v => new Header(h.Key, v))).ToList();
        }

        public static implicit operator HttpRequestMessage(HttpRequestMessageAdapter adapter)
        {
            var headersToAdd = adapter.Headers.Where(newHeader => !adapter.request.Headers.Any(h => h.Key == newHeader.Name));
            foreach (var header in headersToAdd)
            {
                adapter.request.Headers.Add(header.Name, header.Value);
            }

            return adapter.request;
        }

        public HttpRequestMessageAdapter Clone()
        {
            var clonedRequest = new HttpRequestMessage(request.Method, request.RequestUri);

            if (request.Content != null)
            {
                clonedRequest.Content = request.Content;
            }

            clonedRequest.Version = request.Version;
            
            foreach (var property in request.Properties)
            {
                clonedRequest.Properties.Add(property);
            }
            foreach (var header in request.Headers)
            {
                clonedRequest.Headers.TryAddWithoutValidation(header.Key, header.Value);
            }
            
            return new HttpRequestMessageAdapter(clonedRequest);
        }
    }
}
